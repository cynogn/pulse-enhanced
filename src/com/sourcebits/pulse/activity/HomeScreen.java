package com.sourcebits.pulse.activity;

import java.util.Arrays;
import java.util.LinkedList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public final class HomeScreen extends ListActivity {
	public int count = 0;
	String[] mArray = { "1", "2", "3", "4", "5", "6" };
	static final int MENU_MANUAL_REFRESH = 0;
	static final int MENU_DISABLE_SCROLL = 1;
	static final int MENU_SET_MODE = 2;

	private LinkedList<String> mListItems;
	private PullToRefreshListView mPullRefreshListView;
	private ArrayAdapter<String> mAdapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_screen);

		mPullRefreshListView = (PullToRefreshListView) findViewById(R.id.pull_refresh_list);

		// Set a listener to be invoked when the list should be refreshed.
		mPullRefreshListView
				.setOnRefreshListener(new OnRefreshListener<ListView>() {
					public void onRefresh(
							PullToRefreshBase<ListView> refreshView) {
						mPullRefreshListView.setLastUpdatedLabel(DateUtils
								.formatDateTime(getApplicationContext(),
										System.currentTimeMillis(),
										DateUtils.FORMAT_SHOW_TIME
												| DateUtils.FORMAT_SHOW_DATE
												| DateUtils.FORMAT_ABBREV_ALL));

						// Do work to refresh the list here.
						new GetDataTask().execute();
					}
				});

		// Add an end-of-list listener
		mPullRefreshListView
				.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

					public void onLastItemVisible() {
						Toast.makeText(HomeScreen.this, "End of List!",
								Toast.LENGTH_SHORT).show();
					}
				});

		ListView actualListView = mPullRefreshListView.getRefreshableView();

		mListItems = new LinkedList<String>();
		mListItems.addAll(Arrays.asList(mArray));

		mAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, mListItems);

		// You can also just use setListAdapter(mAdapter)
		actualListView.setAdapter(new Adapter(HomeScreen.this, mArray));
		try {
			Thread.sleep(1000);
			startActivity(new Intent(HomeScreen.this, CoverFlowExample.class));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private class GetDataTask extends AsyncTask<Void, Void, String[]> {

		@Override
		protected String[] doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			return mArray;
		}

		@Override
		protected void onPostExecute(String[] result) {
			mListItems.addFirst("Added after refresh...");
			mAdapter.notifyDataSetChanged();

			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}

	class Adapter extends BaseAdapter {
		Context mContext;
		String[] mArrayStrings;

		Adapter(Context context, String[] mArray) {
			mContext = context;
			mArrayStrings = mArray;
		}

		public int getCount() {
			// TODO Auto-generated method stub
			return mArrayStrings.length;
		}

		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int position) {

			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater layoutInflater = null;
			Log.v("Gallery postion", position + "");
			View rowView = convertView;
			if (rowView == null) {
				layoutInflater = ((Activity) mContext).getLayoutInflater();
				rowView = layoutInflater.inflate(R.layout.item, null);
			}
			final Gallery gallery = (Gallery) rowView
					.findViewById(R.id.gallery1);
			gallery.setSpacing(10);
			gallery.setAdapter(new ImageAdapter(mContext));
			return rowView;
		}
	}
}
