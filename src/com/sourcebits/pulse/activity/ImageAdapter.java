package com.sourcebits.pulse.activity;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {

	private int position = 10;
	private Context mContest;

	public ImageAdapter(Context c) {
		mContest = c;
	}

	public int getCount() {
		return position;
	}

	public Object getItem(int arg0) {
		return arg0;
	}

	public long getItemId(int arg0) {
		return arg0;
	}

	public View getView(int pos, View convertView, ViewGroup viewGroup) {
		Log.v("POSTION", pos + "");
		View v;
		LayoutInflater vi = (LayoutInflater) mContest
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = vi.inflate(R.layout.galleryitem, null);
		// ImageView img = (ImageView) v.findViewById(R.id.background1);
		TextView tv = (TextView) v.findViewById(R.id.heading1);
		tv.setText(pos + "");
		return v;
	}
}
