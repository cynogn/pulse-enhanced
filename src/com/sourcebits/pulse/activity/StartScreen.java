package com.sourcebits.pulse.activity;

import com.sourcebits.pulse.database.SQLiteAdapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class StartScreen extends Activity {
	SQLiteAdapter mSqliteAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_screen);
		initializeComponents();
	}

	private void initializeComponents() {
		mSqliteAdapter = new SQLiteAdapter(StartScreen.this);
		mSqliteAdapter.openToWrite();

		startActivity(new Intent(StartScreen.this, HomeScreen.class));
		finish();
	}

	@Override
	protected void onDestroy() {
		mSqliteAdapter.close();
		super.onDestroy();
	}
}
