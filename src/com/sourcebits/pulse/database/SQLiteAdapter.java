package com.sourcebits.pulse.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author GAUTAM BALASUBRAMANIAN
 * 
 */
public class SQLiteAdapter {
	public static final String MYDATABASE_NAME = "PULSE";

	public static final String MYDATABASE_ALLNEWS = "ALLNEWS";

	public static final String MYDATABASE_TABLE = "DASHBOARD";

	public static final String MYDATABASE_ANDROID = "ANDROID";

	public static final String MYDATABASE_APPLE = "APPLE";

	public static final String MYDATABASE_WINDOWS = "WINDOWS";

	public static final int MYDATABASE_VERSION = 1;

	public static final String KEY_COLUMN1 = "LINK";

	public static final String KEY_COLUMN2 = "NAME";

	public static final String KEY_COLUMN4 = "FLAG";

	public static final String KEY_COLUMN3 = "IMAGEURL";

	public static final String KEY_COLUMN5 = "CATEGORY";

	private static final String SCRIPT_CREATE_ALLNEWSCOMMONSCRIPT_PART1 = "create table ";

	private static final String SCRIPT_CREATE_ALLNEWSCOMMONSCRIPT_PART2 = " ("
			+ KEY_COLUMN1 + " text primary key ," + KEY_COLUMN2 + " text,"
			+ KEY_COLUMN5 + " text );";

	private static final String SCRIPT_CREATE_NEWSDASHBOARD_PART1 = "create table ";

	private static final String SCRIPT_CREATE_NEWSDASHBOARD_PART2 = " ("
			+ KEY_COLUMN1 + " text primary key ," + KEY_COLUMN2 + " text ,"
			+ KEY_COLUMN3 + " text ," + KEY_COLUMN4 + " text );";

	private SQLiteHelper sqLiteHelper;
	private SQLiteDatabase sqLiteDatabase;

	/**
	 * contains the context of the activity
	 */
	private Context context;
	/**
	 * contains the cursor
	 */
	private Cursor cursor;

	public SQLiteAdapter(Context c) {
		context = c;
	}

	public SQLiteAdapter openToRead() throws android.database.SQLException {
		sqLiteHelper = new SQLiteHelper(context, MYDATABASE_NAME, null,
				MYDATABASE_VERSION);
		sqLiteDatabase = sqLiteHelper.getReadableDatabase();
		return this;
	}

	public SQLiteAdapter openToWrite() throws android.database.SQLException {
		sqLiteHelper = new SQLiteHelper(context, MYDATABASE_NAME, null,
				MYDATABASE_VERSION);
		sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		sqLiteHelper.close();

	}

	public class SQLiteHelper extends SQLiteOpenHelper {

		public SQLiteHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(SCRIPT_CREATE_ALLNEWSCOMMONSCRIPT_PART1
					+ MYDATABASE_ALLNEWS
					+ SCRIPT_CREATE_ALLNEWSCOMMONSCRIPT_PART2);

			db.execSQL(SCRIPT_CREATE_NEWSDASHBOARD_PART1 + MYDATABASE_ANDROID
					+ SCRIPT_CREATE_NEWSDASHBOARD_PART2);

			db.execSQL(SCRIPT_CREATE_NEWSDASHBOARD_PART1 + MYDATABASE_APPLE
					+ SCRIPT_CREATE_NEWSDASHBOARD_PART2);

			db.execSQL(SCRIPT_CREATE_NEWSDASHBOARD_PART1 + MYDATABASE_WINDOWS
					+ SCRIPT_CREATE_NEWSDASHBOARD_PART2);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		}

	}

}
